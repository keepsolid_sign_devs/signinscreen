import QtQuick 2.6
import QtQuick.Window 2.2

Window {
    visible: true
    width: 370
    height: 528
    maximumWidth: 370
    minimumWidth: 370
    maximumHeight: 528
    minimumHeight: 528
    title: qsTr("KeepSolid Sign")

    MainForm {
        anchors.fill: parent
        mouseArea.onClicked: {
        }
    }
}
