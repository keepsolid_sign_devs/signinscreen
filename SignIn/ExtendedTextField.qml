import QtQuick 2.4

Rectangle {

    property alias lbImage: leftImageID.source
    property alias rbImage: rightImageID.source
    property alias text: textInputID.text
    property alias textColor: textInputID.color
    property alias echoMode: textInputID.echoMode
    property alias textInput: textInputID

    signal rightButtonClicked;
    signal textContentChanged;

    id: extendedTextFieldID
    width: 240
    height: 32

    border.width: 1
    border.color: "red"
    radius: 0

    // 1. left image
    Image {
        id: leftImageID
        y: 0
        width: 32
        height: 32
        scale: 1
        anchors.left: parent.left
        anchors.leftMargin: 0
        source: ""
        fillMode: Image.Pad

            MouseArea {
            id: mouseArea
            x: 0
            y: 0
            width: 32
            height: 32
            }
    }

    // 2. right image
    Image {

        signal rbClicked;

        id: rightImageID
        x: 208
        y: 0
        width: 32
        height: 32
        anchors.right: parent.right
        anchors.rightMargin: 0
        source: ""
        fillMode: Image.Pad

        onRbClicked: parent.rightButtonClicked();

        MouseArea {
            id: mouseArea1
            x: 0
            y: 0
            width: 32
            height: 32
            onClicked: parent.rbClicked();
        }
    }

    // 3. text input
    TextInput {
        id: textInputID
        x: 32
        width: 176
        text: qsTr("")
        font.bold: false
        anchors.bottom: parent.bottom
        anchors.bottomMargin: 0
        anchors.top: parent.top
        anchors.topMargin: 0
        font.weight: Font.Black
        font.family: "Arial"
        horizontalAlignment: Text.AlignLeft
        verticalAlignment: Text.AlignVCenter
        font.pixelSize: 12

        onTextChanged: parent.textContentChanged();
    }
}
