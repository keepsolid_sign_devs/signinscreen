import QtQuick 2.6
import QtQuick.Controls 2.2
import QtQuick.Dialogs 1.1

Rectangle {
    id: rectangle1
    property alias mouseArea: mouseArea

    width: 370
    height: 528

    MouseArea {
        id: mouseArea
        anchors.rightMargin: 0
        anchors.bottomMargin: 0
        anchors.leftMargin: 0
        anchors.topMargin: 0
        anchors.fill: parent

        // 1. Logo image
        Image {
            id: image
            x: 135
            width: 100
            height: 100
            anchors.top: parent.top
            anchors.topMargin: 20
            source: "Images/group.png"
        }

        // 2. "Sign in with KeepSolid ID" textfield
        Text {
            id: text1
            x: 65
            width: 240
            height: 15
            color: "#444444"
            text: qsTr("Sign in with KeepSolid ID")
            renderType: Text.NativeRendering
            horizontalAlignment: Text.AlignHCenter
            anchors.horizontalCenter: parent.horizontalCenter
            anchors.top: parent.top
            anchors.topMargin: 160
            font.pixelSize: 12
        }

        // 3. login textfield
        ExtendedTextField {

            id: loginTextField
            x: 65
            y: 190
            width: 240
            height: 32
            anchors.top: parent.top
            anchors.topMargin: 190
            KeyNavigation.tab: passwordTextField.textInput
            textInput.focus: true

            function isValidEmail(email) {
                var re = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
                return re.test(email);
            }

            text: ""
            lbImage: "Images/noteActive.png"
            rbImage: "" //"Images/iconDelete.png"
            border.color: "#EEEEEE"
            textColor: "#444444"

            onRightButtonClicked: {
                rbImage = ""
                text = ""
            }

            onTextContentChanged: {
                rbImage = (text.length > 0) ? "Images/iconDelete.png" : "";
                loginTextField.border.color = (isValidEmail(text) || text.length == 0) ? (text.length > 0 ? "#22D63A" : "#EEEEEE") : "red";
            }
        }

        // 4. password textfield
        ExtendedTextField {
            id: passwordTextField
            x: 65
            y: 228
            width: 240
            height: 32
            color: "#ffffff"

            KeyNavigation.tab: checkBoxID

            function isPasswordValid(password) {
                return (text.length > 6);
            }

            function isTextLengthValid(text) {
                return (text.length > 6);
            }

            text: ""
            lbImage: "Images/lockIconActive.png"
            rbImage: "" //"Images/iconDelete.png"
            border.color: "#EEEEEE"
            textColor: "#444444"
            echoMode:TextInput.Password

            onRightButtonClicked: {
                rbImage = ""
                text = ""
            }

            onTextContentChanged: {
                rbImage = (text.length > 0) ? "Images/iconDelete.png" : "";
                passwordTextField.border.color = (isTextLengthValid(text) || text.length == 0) ? (text.length > 0 ? "#22D63A" : "#EEEEEE") : "red";
            }
        }

        // 5. "Remember password" checkBox
        CheckBox {
            id: checkBoxID
            width: 162
            height: 19
            //text: "Remember password"
            text: "                 "
            anchors.left: parent.left
            anchors.leftMargin: 115
            anchors.top: parent.top
            anchors.topMargin: 278
            scale: 1
            antialiasing: false
            padding: 0
            leftPadding: 0
            rightPadding: 0
            bottomPadding: 0
            topPadding: 0
            font.pointSize: 12
            font.capitalization: Font.MixedCase
            indicator.width: 16
            indicator.height: 16

            KeyNavigation.tab: signInButtonChildRectID

            Text {
                    text: "      Remember password"
                    font.family: "Arial"
                    color: "#444444"
                    font.pointSize: 12
                    font.capitalization: Font.MixedCase
                    width: 162
                    height: 12

                    y: 2

                }

            onClicked: {
                if (checkBoxID.checked) {

                } else {

                }
            }
        }

        // 6. "SignIn" button
        Button {
            id: signInButtonID
            width: 140
            height: 36
            text: qsTr("Sign In")
            anchors.left: parent.left
            anchors.leftMargin: 115
            anchors.top: parent.top
            anchors.topMargin: 314

            contentItem: Text {
                text: signInButtonID.text
                font: signInButtonID.font
                opacity: enabled ? 1.0 : 0.3
                color: signInButtonID.down ? "#FFFFFF" : "#FFFFFF"
                horizontalAlignment: Text.AlignHCenter
                verticalAlignment: Text.AlignVCenter
                elide: Text.ElideRight
            }

            background: Rectangle {
                id: signInButtonChildRectID
                implicitWidth: 100
                implicitHeight: 40
                opacity: enabled ? 1 : 0.3
                KeyNavigation.tab: forgotPasswordRectangle//forgotPasswordMouseArea
                border.color: focus ? "#146496" : (signInButtonID.down ? "#246EB4" : "#2980CC")
                color: focus ? "#146496" : (signInButtonID.down ? "#246EB4" : "#2980CC")
                border.width: 1
                radius: 2
            }

            onClicked: {
                messageDialogID.visible = true;
                messageDialogID.informativeText = "You have entered\nemail: " + loginTextField.text + "\nPassword: " + passwordTextField.text;
            }
        }

    }

    // 7. forgot password rectangle
    Rectangle {
        id: forgotPasswordRectangle
        x: 130
        y: 390
        width: 110
        height: 20
        focus: true
        color: focus ? "#F0F0F0" : "#ffffff"
        radius: 5

        KeyNavigation.tab: signUpRectangle

        Keys.onPressed: {
            if(event.key == Qt.Key_Space && focus) {
                Qt.openUrlExternally("https://my.keepsolid.com/recovery");
            }
        }

        MouseArea {
            id: forgotPasswordMouseArea
            width: 100
            height: 15
            anchors.left: parent.left
            anchors.leftMargin: 5
            anchors.top: parent.top
            anchors.topMargin: 2

            onClicked: {
                Qt.openUrlExternally("https://my.keepsolid.com/recovery");
            }

            Text {
                id: text2
                x: 0
                y: 0
                width: 100
                color: "#8e8e8e"
                text: qsTr("Forgot password?")
                font.pixelSize: 12
                font.underline: true
            }
        }
    }

    // 8. sign up rectangle
    Rectangle {
        id: signUpRectangle
        x: 100
        y: 432
        width: 170
        height: 20
        color: focus ? "#F0F0F0" : "#ffffff"
        radius: 5
        anchors.left: parent.left
        anchors.leftMargin: 105
        anchors.top: parent.top
        anchors.topMargin: 434

        KeyNavigation.tab: tearmsAndConditionsRectangle

        Keys.onPressed: {
            if(event.key == Qt.Key_Space && focus) {

            }
        }

        MouseArea {
            id: mouseArea3
            x: 0
            y: 0
            width: 170
            height: 20

            onClicked: {

            }

            Text {
                id: text3
                x: 3
                y: 2
                width: 160
                color: "#8e8e8e"
                text: "Don’t have account? <a href='https://'>Sign up!</a>"
                linkColor: "#444444"
                font.pixelSize: 12
            }
        }
    }

    // 9. tearms and conditions rectangle
    Rectangle {
        id: tearmsAndConditionsRectangle
        x: 0
        y: 488
        width: 370
        height: 40
        color: focus ? "#F0F0F0" : "#f9f9f9"
        anchors.bottom: parent.bottom
        anchors.bottomMargin: 0

        KeyNavigation.tab: loginTextField.textInput

        Keys.onPressed: {
            if(event.key == Qt.Key_Space && focus) {
                Qt.openUrlExternally("https://www.keepsolid.com/terms");
            }
        }

        Rectangle {
            id: rectangle2
            x: 0
            y: 0
            width: 370
            height: 1
            color: "#ebebeb"
        }

        MouseArea {
            id: mouseArea1
            x: 0
            y: 2
            width: 370
            height: 40

            Text {
                id: tearmsAndConditionstext
                x: 0
                y: 12
                width: 370
                height: 15
                color: "#444444"
                text: "By selecting Sign In, you agree to the <a href='https://www.keepsolid.com/terms'>Tearms and Conditions</a>"
                font.capitalization: Font.Capitalize
                font.family: "Arial"
                horizontalAlignment: Text.AlignHCenter
                font.pixelSize: 11
                linkColor: "#444444"

                //KeyNavigation.tab: loginTextField
                //KeyNavigation.backtab: signUpRectangle
            }

            onClicked: {
                Qt.openUrlExternally("https://www.keepsolid.com/terms");
            }
        }

    }

    // 10. message dialog
    MessageDialog {
        id: messageDialogID
        visible: falce
        modality: Qt.Normal
        title: "Message"
    }
}
